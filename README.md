# UnLUFed YouTube

Recently i had purchased MMX 300s and noticed that it just didn't sound right with audio coming from YouTube, So i decided to do some digging.

It turns out YouTube actually [normalizes audio streams](https://www.meterplugs.com/blog/2019/09/18/youtube-changes-loudness-reference-to-14-lufs.html) so you don't have to change your volume frequently (According to them), This actually led me to change my volume more for on and off YouTube and got really annoying so i decided to make a [TamperMonkey](https://www.tampermonkey.net/) script to denormalize audio from YT.

Add by clicking "Open Raw" with TamperMonkey installed. ![OpenRawImg](https://i.imgur.com/T6mOpFF.png)
